import { createApp } from 'vue'
import App from './App.vue'
import PrimeVue from 'primevue/config';
import './assets/css/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import Toast from 'primevue/toast';
import ToastService from 'primevue/toastservice';
const app = createApp(App);
app.use(PrimeVue);
app.use(ToastService);
app.component('Toast', Toast);
app.mount("#app");


