# MASIV

## Name
Este proyecto es una Test

## Description

Por favor escriba un programa utilizando JS, HTML y CSS, que cumpla con los siguientes
requerimientos funcionales:

1. Consumir el API de xkcd: https://xkcd.com/json.html
2. Crear la siguiente pantalla mostrando un comic aleatorio de xkcd.
3. Permitir que el usuario califique el comic.


## Installation

1. para el correcto funcionamiento de este proyecto es nesesario NODE.JS (https://nodejs.org/es/)
2. tenga que este proyecto corre sobre los puertos 3000 y 4000 asegurese que estes disponibles en su equipo.
3. clone el repositorio
    ```
    cd existing_repo
    git remote add origin https://gitlab.com/ph_jegomez/masiv.git
    npm install
    ```
4. despues de clonar debera instalar las depencias del PROXY para el consumo del web service. posicionase en la raiz del proyecto y ejecute
    ```
    cd /proxy
    npm i
    npm run start
    ```
5. Iniciar el servidor. posicionarse en la raiz del proyecto y ejecute
    ```
    npm run dev 
    ```
## Support

si tiene problemas comuniquese al +57-318-831-5485 o go.juangomez23@gmial.com

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Project status

