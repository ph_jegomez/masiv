const http = require("https");
const express = require("express");
const app = express();
const cors = require('cors');
app.use(cors());
app.listen(4000, () => console.log("Server started on port 4000"));
app.get("/", (request, respose) => {
    const options = {
        "method": "GET",
        "hostname": "xkcd.com",
        "port": null,
        "path": "/info.0.json",
        "headers": {
            "Content-Length": "0"
        }
    };
    const req = http.request(options, function (res) {
        const chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            const body = Buffer.concat(chunks);
            respose.send(body.toString());
        });
    });
    req.end();
});
